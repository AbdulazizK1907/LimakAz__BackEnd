﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Dto
{
    public class OrderDto
    {
        public int Id { get; set; }

        public string ButtonName { get; set; }

        public string ButtonUrl { get; set; }
    }
}
