﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Dto
{
    public class AuxiliarySectionDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }
    }
}
