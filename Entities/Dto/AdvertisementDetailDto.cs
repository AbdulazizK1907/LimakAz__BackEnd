﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Dto
{
    public class AdvertisementDetailDto
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public int AdvertisementId { get; set; }
    }
}
