﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Dto
{
    public class WeightInputDto
    {
        public int Id { get; set; }

        public string InputName { get; set; }
    }
}
